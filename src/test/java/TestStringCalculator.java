import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestStringCalculator {

    private StringCalculator sc;
    @BeforeEach
    public void setUp() {
        sc = new StringCalculator();
    }
    @Test
    void GivenInputIsEmptyStringWhenAddMethodIsCalledShouldReturn0(){
        assertEquals(0, sc.add(""));
    }

    @Test
    void GivenInputIsASingleLengthStringContainingNumberWhenAddMethodIsCalledShouldReturnTheNumber(){
        assertEquals(1, sc.add("1"));
        assertEquals(0, sc.add("0"));
    }

    @Test
    void GivenInputStringIsNumberCommaNumberWhenAddMethodIsCalledShouldReturnSumOfNumbers(){
        assertEquals(3, sc.add("1,2"));
    }

    @Test
    void GivenInputContainsUnknownNumberofArgumentsWhenAddMethodIsCalledShouldReturnSumOfNumbers(){
        assertEquals(6, sc.add("1,2,3"));
        assertEquals(74, sc.add("12,23,39"));
        assertEquals(30, sc.add("1,2,3,9,8,7"));
    }

    @Test
    void GivenInputContainsNewLineCharacterWhenAddMethodIsCalledShouldReturnSumOfNumbers(){
        assertEquals(6, sc.add("1\n2,3"));
    }

    @Test
    void GivenInputStringContainsDelimiterInTheEndWhenAddMethodIsCalledShouldThrowException(){
        assertThrows(IllegalArgumentException.class, () -> sc.add("1,2\n"));
        assertThrows(IllegalArgumentException.class, () -> sc.add("1,22,3,"));
    }

    @Test
    void GivenInputContainsDifferentDelimitersWhenAddMethodIsCalledShouldReturnSumOfNumbers(){
        assertEquals(4, sc.add("//;\n1;3"));
        assertEquals(6, sc.add("//|\n1|2|3"));
        assertEquals(7, sc.add("//sep\n2sep5"));
        assertThrows(RuntimeException.class, () -> sc.add("//|\n1|2,3"));
    }

    @Test
    void GivenInputContainsNegativeNumberWhenAddMethodIsCalledShouldReturnErrorMessage(){
        assertThrows(IllegalArgumentException.class, () -> sc.add("1,-2"));
        assertThrows(IllegalArgumentException.class, () -> sc.add("2,-4,-9"));
        var result = assertThrows(IllegalArgumentException.class, () -> sc.add("1,-2") );
        assertEquals("Negatives numbers(s) not allowed: -2,", result.getMessage());

        result = assertThrows(IllegalArgumentException.class, () -> sc.add("2,-4,-9") );
        assertEquals("Negatives numbers(s) not allowed: -4,-9,", result.getMessage());
    }
}
