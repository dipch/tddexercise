import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPointOfSaleKata {

    private PointOfSaleKata pointOfSaleKata;

    @BeforeEach
    public void setUp() {
        pointOfSaleKata = new PointOfSaleKata();
    }

    @Test
    public void GivenInput12345ShouldReturn725WhenBarcodeMethodIsCaled(){
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(12345));
        assertEquals("$7.25", pointOfSaleKata.total(expected));
    }

    @Test
    public void GivenInput23456ShouldReturn1250WhenBarcodeMethodIsCaled(){
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(23456));
        assertEquals("$12.5", pointOfSaleKata.total(expected));
    }

    @Test
    public void GivenInput99999ShouldReturnErrorWhenBarcodeMethodIsCaled(){
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(99999));
        assertEquals("Error: empty barcode", pointOfSaleKata.total(expected));
    }

    @Test
    public void GivenInput12345And23456ShouldReturnTotalWhenBarcodeMethodIsCaled(){
        List<Integer> expected = new ArrayList<Integer>(Arrays.asList(12345,23456));
        assertEquals("$19.75", pointOfSaleKata.total(expected));
    }


}
