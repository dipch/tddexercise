import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestFizzBuzz {
    @Test
    void shouldReturnInputNumberAsStringWhenFizzBuzzMethodIsCalled(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("1", fizzBuzz.fizzBuzz(1));
        assertEquals("2", fizzBuzz.fizzBuzz(2));
        assertEquals("97", fizzBuzz.fizzBuzz(97));
    }

    @Test
    void GivenInputNumberIsMultipleOfThreeWhenFizzBuzzMethodIsCalledFizzIsReturned(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Fizz", fizzBuzz.fizzBuzz(3));
        assertEquals("Fizz", fizzBuzz.fizzBuzz(6));
        assertEquals("Fizz", fizzBuzz.fizzBuzz(99));
    }

    @Test
    void GivenInputNumberIsMultipleOfFiveWhenFizzBuzzMethodIsCalledBuzzIsReturned(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("Buzz", fizzBuzz.fizzBuzz(5));
        assertEquals("Buzz", fizzBuzz.fizzBuzz(10));
        assertEquals("Buzz", fizzBuzz.fizzBuzz(95));
    }

    @Test
    void GivenInputNumberIsMultipleOfThreeAndFiveWhenFizzBuzzMethodIsCalledFizzBuzzIsReturned(){
        FizzBuzz fizzBuzz = new FizzBuzz();
        assertEquals("FizzBuzz", fizzBuzz.fizzBuzz(15));
        assertEquals("FizzBuzz", fizzBuzz.fizzBuzz(30));
        assertEquals("FizzBuzz", fizzBuzz.fizzBuzz(45));
    }
}
