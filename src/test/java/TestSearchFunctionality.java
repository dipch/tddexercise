import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSearchFunctionality {
    private SearchFunctionality searchFunctionality;


    @BeforeEach
    public void init() {
        searchFunctionality = new SearchFunctionality();
    }

    @Test
    public void GivenSearchTextFewerThan2CharShouldReturnEmptyStringWhenSearchMethodIsCalled(){
        List<String> expected = new ArrayList<String>();
        assertEquals(expected, searchFunctionality.search("a"));
    }

    @Test
    public void GivenSearchTextContainsAsteriskShouldReturnEmptyStringWhenSearchMethodIsCalled(){
        List<String> expected = new ArrayList<String>(Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valencia", "Vancouver", "Amsterdam", "Vienna", "Sydney", "New York City", "London", "Bangkok", "Hong Kong", "Dubai", "Rome", "Istanbul"));
        assertEquals(expected, searchFunctionality.search("*"));
    }

    @Test
    public void GivenSearchTextContainsRShouldReturnRotterdamWhenSearchMethodIsCalled() {
        List<String> expected = new ArrayList<String>(Arrays.asList("Rotterdam"));
        assertEquals(expected, searchFunctionality.search("rot"));
    }

    @Test
    public void GivenSearchTextContainsPartOfNameShouldReturnRotterdamWhenSearchMethodIsCalled() {
        List<String> expected = new ArrayList<String>(Arrays.asList("Rotterdam"));
        assertEquals(expected, searchFunctionality.search("otter"));
    }



}
