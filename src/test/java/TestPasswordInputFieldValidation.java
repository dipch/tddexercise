import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestPasswordInputFieldValidation {



    private PasswordInputFieldValidation passwordInputFieldValidation;
    @BeforeEach
    public void setUp() {
        passwordInputFieldValidation = new PasswordInputFieldValidation();
    }


    @Test
    public void GivenInputLessThan8CharacterShouldReturnExceptionWhenValidateMethodIsCalled() {
        var result = assertThrows(IllegalArgumentException.class, () -> passwordInputFieldValidation.validate("pA55w$r"));
        assertEquals("Password must be at least 8 characters\n", result.getMessage());
    }

    @Test
    public void GivenInputContainsLessThan2NumbersShouldReturnExceptionWhenValidateMethodIsCalled() {
        var result = assertThrows(IllegalArgumentException.class, () -> passwordInputFieldValidation.validate("pA$ss5word"));
        assertEquals("Password must contain at least 2 numbers\n", result.getMessage());
    }

    @Test
    public void GivenInputCDoesNotContainOneCapitalLetterShouldThrowExceptionWhenValidateMethodIsCalled() {
        var result = assertThrows(IllegalArgumentException.class, () -> passwordInputFieldValidation.validate("pas$5w7ord"));
        assertEquals("Password must contain at least 1 capital letter\n", result.getMessage());
    }

    @Test
    public void GivenInputCDoesNotContainSpecialCharacterShouldThrowExceptionWhenValidateMethodIsCalled() {
        var result = assertThrows(IllegalArgumentException.class, () -> passwordInputFieldValidation.validate("paS65word"));
        assertEquals("Password must contain at least 1 special character\n", result.getMessage());
    }

    @Test
    public void GivenInputWithMultipleErrorShouldThrowExceptionWhenValidateMethodIsCalled() {
        var result = assertThrows(IllegalArgumentException.class, () -> passwordInputFieldValidation.validate("pass"));
        assertEquals("Password must be at least 8 characters\nPassword must contain at least 2 numbers\nPassword must contain at least 1 capital letter\nPassword must contain at least 1 special character\n", result.getMessage());
    }

    @Test
    public void GivenValidInputShouldReturnTrueWhenValidateMethodIsCalled() {
        assertEquals(true, passwordInputFieldValidation.validate("pa$5w7oRd"));
    }

}


























//    String psswd = "utjlkfj";
//    var exception = assertThrows(IllegalArgumentException.class, () -> tdd.validator(psswd));
//    assertEquals("Password must be at least 8 characters", exception.getMessage());
