public class PasswordInputFieldValidation {
    public Boolean validate(String s) {

        StringBuilder sb = new StringBuilder();
        Boolean throwsException = false;

        if(s.length() < 8) {
            throwsException = true;
            sb.append("Password must be at least 8 characters\n");
        }



        int count = 0;
        for(int i = 0; i < s.length(); i++) {
            if(Character.isDigit(s.charAt(i))) {
                count++;
            }
        }
        if(count < 2) {
            throwsException = true;
            sb.append("Password must contain at least 2 numbers\n");
        }


//        if(!s.matches(".*\\d.*")) {
//            throwsException = true;
//            sb.append("Password must contain at least 2 numbers\n");
//        }

//        if (!s.matches(".*[0-9].*[0-9]{2,}")) {
//            throwsException = true;
//            sb.append("Password must contain at least 2 numbers\n");
//        }
        if (!s.matches(".*[A-Z].*")) {
            throwsException = true;
            sb.append("Password must contain at least 1 capital letter\n");
        }
        if (!s.matches(".*[^a-zA-Z0-9].*")) {
            throwsException = true;
            sb.append("Password must contain at least 1 special character\n");
        }

        if(throwsException) {
            throw new IllegalArgumentException(sb.toString());
        }
        return true;
    }
}
