import java.util.ArrayList;
import java.util.List;

public class StringCalculator {
    public int add(String numbers) {
        if (numbers.length() == 0){
            return 0;
        }
        else if (numbers.length() == 1){
            return Integer.parseInt(numbers);
        }
        else if (numbers.length() == 3){
            String[] tokens = numbers.split(",");
            int result =0;
            for (String t : tokens){
                result += Integer.parseInt(t);
            }
            return result;
        }
        else if (numbers.length() > 3){

            if (numbers.startsWith("//")){
                int indexOfLastDelimiter = numbers.indexOf("\n");
                String delimiter = numbers.substring(2,indexOfLastDelimiter);
                String newNumbers = numbers.substring(indexOfLastDelimiter+1, numbers.length());
//                System.out.println("n =  "+numbers);
//                System.out.println("delimiter--->"+delimiter);
//                System.out.println("number length--->"+numbers.length());
                System.out.println("new number--->"+newNumbers);

                if(newNumbers.contains(";") && (newNumbers.contains(",") || newNumbers.contains("|"))){
                    throw new RuntimeException(delimiter+ " expected but "+"another delimiter found ");
                }

                if(newNumbers.contains(",") && (newNumbers.contains(";") || newNumbers.contains("|"))){
                    throw new RuntimeException(delimiter+ " expected but "+"another delimiter found ");
                }

                if(newNumbers.contains("|") && (newNumbers.contains(",") || newNumbers.contains(";"))){
                    throw new RuntimeException(delimiter+ " expected but "+"another delimiter found ");
                }


                String[] tokens;
                if (delimiter.equals("|")){
                    tokens = newNumbers.split("\\|");
                }
                else{
                    tokens = newNumbers.split(delimiter);
                }

                int result =0;
                for (String t : tokens){
                    System.out.println("t--->"+t);
                    result += Integer.parseInt(t);
                }

                //System.out.println("result--->"+result);
                return result;
            }

            if (numbers.endsWith("\n") || numbers.endsWith(",")){
                throw new IllegalArgumentException("New line or comma at the end of the string");
            }

            String[] tokens = numbers.split("[,\n]");
            int result =0;
            Boolean negativeCheck = false;
            List<String> ar = new ArrayList<String>();
            for (String t : tokens){

                if(Integer.parseInt(t)<0){
                    negativeCheck = true;
                    ar.add(t);
                    continue;
                }
                if(Integer.parseInt(t)>1000){
                    continue;
                }
                result += Integer.parseInt(t);
            }
            if (negativeCheck){
                StringBuilder sb = new StringBuilder();
                for(String s : ar){
                    sb.append(s);
                    sb.append(",");
                }
                throw new IllegalArgumentException("Negatives numbers(s) not allowed: "+ sb);
            }
            return result;
        }
        return 0;
    }
}
