import java.util.List;

public class PointOfSaleKata {

    public String total(List<Integer> list) {
        double sum = 0.0;
        StringBuilder sb = new StringBuilder();
        for (int s : list) {
            if(s == 12345){
                sum += 7.25;

            }
            else if(s == 23456){
                sum += 12.50;

            }
            else if(s == 99999){
                return "Error: empty barcode";
            }




        }
        sb.append("$");
        sb.append(sum);
        return sb.toString();
    }
}
