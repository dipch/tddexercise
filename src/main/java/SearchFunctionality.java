import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchFunctionality {
    public List<String> list = new ArrayList<String>(Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valencia", "Vancouver", "Amsterdam", "Vienna", "Sydney", "New York City", "London", "Bangkok", "Hong Kong", "Dubai", "Rome", "Istanbul"));

    public List<String> search(String a) {
        if(a.equals("*")) {
            return list;
        }
        if(a.length()<2) {
            return new ArrayList<String>();
        }
        if(a.length()>=2) {

            List<String> result = new ArrayList<String>();
            for(String s : list) {
                if(s.startsWith(a) || s.toLowerCase().startsWith(a) || s.contains(a) || s.toLowerCase().contains(a)) {
                    result.add(s);
                }
            }
            return result;
        }
        return list;
    }

}



